require('dotenv').config()

const express = require('express')
const restful = require('node-restful')
const bodyParser = require('body-parser')
const cors = require('cors')

const server = express()
const mongoose = restful.mongoose

//Database
mongoose.Promise = global.Promise
mongoose.connect(process.env.DB_STRING)

// server.get('/', (req, res, next) => res.send('Backend'))

//Middleware
server.use(bodyParser.urlencoded({extended:true}))
server.use(bodyParser.json())
server.use(cors())

//ODM
const Client = restful.model('Client', {
    name: { type: String, required: true }
})

//Rest API
Client.methods(['get', 'post', 'put', 'delete'])
Client.updateOptions({ new: true, runValidators: true })

//Routes
Client.register(server, '/clients')

server.listen(3000)